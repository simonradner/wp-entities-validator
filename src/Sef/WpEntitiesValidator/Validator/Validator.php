<?php
namespace Sef\WpEntitiesValidator\Validator;
use Sef\WpEntitiesValidator\Interfaces\ValidatorInterface;
use Sef\WpEntities\Base\Entitybag;


class Validator extends BaseValidator implements ValidatorInterface {

  protected $targetEntity;

  protected $data;

  protected $allowedProps = [];

  public function reset( )
  {
    $this->data = null;
    $this->targetEntity = null;
    $this->allowedProps = null;
    return $this;
  }

  public function setData( $data )
  {
    $this->data = $data;
    return $this;
  }

  public function setTargetEntity( Entitybag $entity )
  {
    $this->targetEntity = $entity;
    return $this;
  }

  public function getTargetEntity()
  {
    return $this->targetEntity;
  }

  public function setAllowedProps( array $props )
  {
    $this->allowedProps = $props;
    // $exporterStrategy = $this->options['exporterStrategy'];
    // $exporterStrategy->setArgs($props);
    // $this->options['exporterStrategy'] = $exporterStrategy;
    $importerStrategy = $this->options['importerStrategy'];
    $importerStrategy->setArgs($props);
    $this->options['importerStrategy'] = $importerStrategy;
    return $this;
  }

  public function validate()
  {
    $importerStrategy = $this->options['importerStrategy'];
    $exporterStrategy = $this->options['exporterStrategy'];
    $target = $this->targetEntity;
    $data = $this->data;
    $validator = $this->options['symfonyValidator'];

    if( $target )
    {
      if( $data )
      {
        $target->import($data, $importerStrategy );
        $this->targetEntity = $target;
      }
       $exported = $target->export( $exporterStrategy );
       return $validator->validate($exported);
    }
  }
}
