<?php
namespace Sef\WpEntitiesValidator\Validator;
use Sef\WpEntitiesValidator\Interfaces\ValidatorImporterStrategyInterface;
use Sef\WpEntitiesValidator\Interfaces\ValidatorInterface;
use Sef\WpEntitiesValidator\ValidatorImporter\PropertyNameSetterIncludeValidatorImporterStrategy;
use Sef\WpEntities\Base\Entitybag;
use Sef\WpEntities\Components\Exporter\ExcludeExporterStrategy;
use Sef\WpEntities\Interfaces\ExporterStrategyInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\Options;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\Reader;

abstract class BaseValidator {

  /**
   * @var bool
   */
  protected static $loaderRegistered = false;

  /*
   * @var array
   */
  protected $options;

  /*
   * @var OptionsResolver
   */
  protected $optionResolver;

  public function __construct( array $options = [] )
  {
    $this->buildOptions($options);
    if( ! static::$loaderRegistered )
    {
      $symfonyConstraintsDir = $this->options['symfonyConstraintsDir'];
      static::registerLoader($symfonyConstraintsDir);
    }
  }

  public function setExporterStrategy( ExporterStrategyInterface $strategy )
  {
    $options = $this->options;
    $options['exporterStrategy'] = $strategy;
    $this->options = $this->optionResolver->resolve( $options );
  }

  public function setImporterStrategy( ValidatorImporterStrategyInterface $strategy )
  {
    $options = $this->options;
    $options['importerStrategy'] = $strategy;
    $this->options = $this->optionResolver->resolve( $options );
  }

  protected function buildOptions( array $options )
  {
    $resolver = new OptionsResolver();

    $resolver->setRequired([
      'symfonyConstraintsDir',
      'annotationReader',
      'symfonyValidator',
      'exporterStrategy',
      'importerStrategy'
    ]);

    $resolver->setAllowedTypes('symfonyConstraintsDir', 'string' );
    $resolver->setAllowedTypes('annotationReader', 'Doctrine\Common\Annotations\Reader' ); // Interface: Doctrine\Common
    $resolver->setAllowedTypes('symfonyValidator', 'Symfony\Component\Validator\Validator\ValidatorInterface' );
    $resolver->setAllowedTypes('exporterStrategy', 'Sef\WpEntities\Interfaces\ExporterStrategyInterface' );
    $resolver->setAllowedTypes('importerStrategy', 'Sef\WpEntitiesValidator\Interfaces\ValidatorImporterStrategyInterface' );

    $resolver->setDefault('symfonyConstraintsDir', dirname(dirname(dirname(dirname(dirname(__DIR__))))) . '/symfony/validator/Constraints/');
    $resolver->setDefault('annotationReader', function (Options $options) {
      return static::createDefaultAnnotationReader();
    });
    $resolver->setDefault('symfonyValidator', function (Options $options) {
      $annotationReader = $options['annotationReader'];
      return static::createDefaultSymfonyValidator($annotationReader);
    });
    $resolver->setDefault('exporterStrategy', function (Options $options) {
      return static::createDefaultExporterStrategy();
    });
    $resolver->setDefault('importerStrategy', function (Options $options) {
      return static::createDefaultImporterStrategy();
    });
    $this->optionResolver = $resolver;
    $this->options = $resolver->resolve($options);
  }

  protected static function createDefaultAnnotationReader()
  {
    return new AnnotationReader();
  }

  protected static function createDefaultSymfonyValidator( Reader $annotationReader )
  {
    return \Symfony\Component\Validator\Validation::createValidatorBuilder()
      ->enableAnnotationMapping($annotationReader)
      ->addMethodMapping('loadValidatorMetadata')
      ->getValidator();
  }

  protected static function createDefaultExporterStrategy( )
  {
    return new ExcludeExporterStrategy;
  }

  protected static function createDefaultImporterStrategy( )
  {
    return new PropertyNameSetterIncludeValidatorImporterStrategy;
  }

  protected static function registerLoader( $symfonyConstraintsDir )
  {
    // Validator Annotation Autoload
    AnnotationRegistry::registerLoader(function($class) use($symfonyConstraintsDir)  {
      $file = str_replace("\\", DIRECTORY_SEPARATOR, $class) . ".php";
      if (strpos($file, "Symfony/Component/Validator/Constraints/") === 0) {

        $file = str_replace("Symfony/Component/Validator/Constraints/", '', $file);
        if (file_exists(trailingslashit($symfonyConstraintsDir) . $file)) {
          // file exists makes sure that the loader fails silently
          require trailingslashit($symfonyConstraintsDir) . $file;
          return true;
        }
      }
    });
    //   'symfonyConstraintsDir' => VENDOR_PATH . '/symfony/symfony/src/Symfony/Component/Validator/Constraints/'

    static::$loaderRegistered = true;
  }
}
