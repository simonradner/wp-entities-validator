<?php
namespace Sef\WpEntitiesValidator\Interfaces;
use Sef\WpEntitiesValidator\Interfaces\ValidatorImporterStrategyInterface;
use Sef\WpEntities\Base\Entitybag;
use Sef\WpEntities\Interfaces\ExporterStrategyInterface;

interface ValidatorInterface
{
  function setData( $data );
  function setTargetEntity( Entitybag $entity );
  function getTargetEntity( );
  function setAllowedProps( array $props );
  function setExporterStrategy( ExporterStrategyInterface $strategy );
  function setImporterStrategy( ValidatorImporterStrategyInterface $strategy );
  function validate();
  function reset();
}
