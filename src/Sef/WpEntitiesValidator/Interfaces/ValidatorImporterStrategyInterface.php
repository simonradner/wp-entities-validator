<?php
namespace Sef\WpEntitiesValidator\Interfaces;
use Sef\WpEntities\Interfaces\ImporterStrategyInterface;

interface ValidatorImporterStrategyInterface extends ImporterStrategyInterface
{
  function setArgs( array $args = []);
}
