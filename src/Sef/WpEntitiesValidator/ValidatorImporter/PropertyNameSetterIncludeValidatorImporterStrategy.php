<?php
namespace Sef\WpEntitiesValidator\ValidatorImporter;

use Sef\WpEntitiesValidator\Interfaces\ValidatorImporterStrategyInterface;
use Sef\WpEntities\Base\EntityBag;

class PropertyNameSetterIncludeValidatorImporterStrategy implements ValidatorImporterStrategyInterface {

  protected $includes = [];

  public function __construct( array $includes = [] )
  {
    $this->includes = $includes;
  }

  public function setArgs( array $includes = [])
  {
    $this->includes = $includes;    
  }

  public function import( $data, EntityBag $to )
  {
    if( ! is_array( $data ))
    {
      // cant import anything
      return;
    }

    foreach( $this->includes as $include )
    {
      $setter = 'set' . ucfirst($include);
      if( array_key_exists( $include, $data ) ) {
        // do import
        $to->$setter($data[$include]);
      }
    }
  }
}
